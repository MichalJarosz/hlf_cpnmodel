# HLF_CPNModel
Models for testing the performance of Hypelredger Fabric networks using CPN Tools.


## Files
HLF_2org.cpn - model for Hyperledger Fabric with 2 organizations

HLF_3org.cpn - model for Hyperledger Fabric with 3 organizations
